﻿using System;
using System.Collections.Generic;
using UnityEngine;


    public class Gun : MonoBehaviour
    {
    [SerializeField] private float range;
    [SerializeField] private float bulletForce = 20f;
    [SerializeField] private float reloadTime;
    [SerializeField] private float explosionRange;

    [SerializeField] private int maxClip;
    [SerializeField] private int maxAmmo;


    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private GameObject impactEffeckt;


    [SerializeField] private Transform firePoint;

        // Start is called before the first frame update

        //Update is called once per frame
        void Update()
        {
        if (Input.GetMouseButtonDown(0))
        {
           Shoot();
        }
        }
       private void Shoot()
        {
            Debug.Log("i'm shooting");
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
        }
    }


