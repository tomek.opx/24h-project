﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granade : MonoBehaviour
{
    [SerializeField] private GameObject explosion;
    [SerializeField] private bool isBomb;
    [SerializeField] private float damage = 10;


    /* private void OnCollisionEnter2D(Collision2D collision)
     {
         if (isBomb)
         {
             GameObject effect = Instantiate(explosion, transform.position, Quaternion.identity);
             Destroy(effect, 5f);
             //take damage
             Destroy(gameObject);

         }
         else
         {
             //take damage
             Debug.Log("you hit ");
             Destroy(gameObject);

         }
     }*/
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("You hit " + collision.name);

            collision.GetComponent<Enemy>().TakeDamage(damage);
        }
    }
}
