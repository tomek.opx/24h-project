﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private GameObject player;

    [SerializeField] private float spawnRate = 2;

    public int enemyNumber;

    int enemyToSpawn;
    float nextSpawn = 0.0f;
    float spawnX;
    float spawnY;
    Vector2 whereToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        enemyToSpawn = enemyNumber;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            spawnX = player.transform.position.x + 5;
            spawnY = player.transform.position.y + 5;

            whereToSpawn = new Vector2(spawnX, spawnY);
            Instantiate(enemyPrefab, whereToSpawn, Quaternion.identity);
        }
    }
}
