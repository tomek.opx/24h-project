﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Input
{
    public class PlayerMovement : MonoBehaviour
    {
        public float moveSpeed = 5f;

        public Rigidbody2D rb;

        public Camera cam;

        Vector2 movement;
        Vector2 mousePos;


        // Update is called once per frame
        void Update()
        {
            movement.x = UnityEngine.Input.GetAxisRaw("Horizontal");
            movement.y = UnityEngine.Input.GetAxisRaw("Vertical");

            mousePos = cam.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
      }
        private void FixedUpdate()
        {
            rb.MovePosition(rb.position + movement * moveSpeed * Time.deltaTime);

            Vector2 lookDirection = mousePos - rb.position;
            float angle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg - 90f;

            rb.rotation = angle;
        }
    }

}
